# MAUVE Unicycle Control package

This package contains MAUVE components for control of unicycle robots

This package is under GPL License.

## Installation instructions

```bash
mkdir -p ~/mauve_ws/src
cd ~/mauve_ws
git clone git@gitlab.com:MAUVE-wip/mauve_unicycle_control.git src/mauve_unicycle_control
catkin_make
source devel/setup.bash
```
