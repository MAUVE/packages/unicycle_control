/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Unicycle Control project.
 *
 * MAUVE Unicycle Control is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Unicycle Control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#include <limits>
#include <vector>

#include "unicycle_control/mauve/SafetyPilot.hpp"

namespace unicycle_control {
  namespace mauve {

    using namespace ::mauve::types::geometry;

void SafetyPilotCore::read_scan() {
  auto s = shell().scan.read();
  obstacle_distance = std::numeric_limits<double>::max();
  obstacle_lateral_distance = std::numeric_limits<double>::max();

  for (int i = 0; i < s.ranges.size(); i++) {
    double r = s.ranges[i];
    if (r > s.range_min) {
      double a = s.angle_min + (s.angle_max - s.angle_min) * i / s.ranges.size();
      double dy = fabs( r * sin(a) );
      if (dy <= shell().stop_lateral_distance) {
        obstacle_distance = std::min(obstacle_distance, r);
        obstacle_lateral_distance = std::min(obstacle_lateral_distance, dy);
      }
    }
  }
  logger().debug("Obstacle at {} (lateral: {})", obstacle_distance, obstacle_lateral_distance);
  shell().obstacle_distance.write(obstacle_distance);
}

void SafetyPilotCore::saturate_input() {
  input = shell().input.read();
  input.linear = fmin(input.linear, + shell().max_linear);
  input.linear = fmax(input.linear, - shell().max_linear);
  input.angular = fmin(input.angular, + shell().max_angular);
  input.angular = fmax(input.angular, - shell().max_angular);
}

bool SafetyPilotCore::obs_too_close() {
  return obstacle_distance < shell().stop_distance;
}

void SafetyPilotCore::stop() {
  if (shell().emergency_backward.get()) {
    logger().warn("Obstacle too close: robot is not allowed to go forward");
    input.linear = std::min(0.0, input.linear);
  } else {
    logger().warn("Obstacle too close: robot is not allowed to go forward/backward");
    input.linear = 0.0;
  }
  shell().output.write(input);
}

void SafetyPilotCore::forward() {
  shell().output.write(input);
}

bool SafetyPilotFSM::configure_hook() {
  set_initial(read);
  set_next(read, saturate);
  mk_transition(saturate, &SafetyPilotCore::obs_too_close, emergency);
  set_next(saturate, safe);
  set_next(emergency, sync);
  set_next(safe, sync);
  set_next(sync, read);

  sync.set_clock(period.get());

  return true;
}

}} // namespaces
