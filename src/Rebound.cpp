/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Unicycle Control project.
 *
 * MAUVE Unicycle Control is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Unicycle Control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#include "mauve/unicycle_control/Rebound.hpp"

namespace mauve {
  namespace unicycle_control {

void ReboundCore::update() {
  robot_pose = shell().pose.read();
  logger().debug("Robot pose: {}", robot_pose);

  goal = shell().goal.read().value;

  double dist = sqrt( pow( robot_pose.location.x - goal.x , 2)
    + pow( robot_pose.location.y - goal.y , 2) );
  if (dist < shell().goal_tolerance) {
    logger().debug("At goal");
    shell().command.write( {0, 0} );
    return;
  }

  types::geometry::Point2D target;
  if (obstacleFromScan(shell().scan.read()))
    target = reboundPoint();
  else
    target = goal;

  auto cmd = command(robot_pose, target);
  logger().debug("Command: {}", cmd);
  shell().command.write(cmd);
}

bool ReboundCore::obstacleFromScan(const types::sensor::LaserScan& s) {
  bool obstacle = false;
  alpha = 0;
  d = 0;

  for (int i = 0; i < s.ranges.size(); i++) {
	  double di = s.ranges[i];
		if (di < s.range_min) continue;
		if (di > s.range_max) di = s.range_max;
		if (di <= shell().obstacle_distance) obstacle = true;
		double ai = s.angle_min + i * (s.angle_max - s.angle_min) / s.ranges.size();
		alpha += di * ai;
		d += di;
	}
  logger().debug("alpha: {}, d: {}", alpha, d);

  return obstacle;
}

types::geometry::Point2D ReboundCore::reboundPoint() {
  types::geometry::Point2D B;
  double a = alpha / d;
	double r = 1;//sqrt( pow( robot_pose.location.x - goal.x , 2)
    //+ pow( robot_pose.location.y - goal.y , 2) );
	double dx = r * cos(a + robot_pose.theta);
	double dy = r * sin(a + robot_pose.theta);
	B.x = robot_pose.location.x + dx;
  B.y = robot_pose.location.y + dx;
  logger().debug("rebound point: {}", B);
	return B;
}

types::geometry::UnicycleVelocity ReboundCore::command(
  const types::geometry::Pose2D& p,
  const types::geometry::Point2D& g)
{
  double dx = g.x - p.location.x;
  double dy = g.y - p.location.y;
  double heading = p.theta;
  double route = atan2(dy, dx);

  // angular velocity
  double ang_error = route - heading;
  while( fabs(ang_error + 2*M_PI) <= fabs(ang_error) ) ang_error += 2*M_PI;
	while( fabs(ang_error - 2*M_PI) < fabs(ang_error) ) ang_error -= 2*M_PI;
  double w = ang_error;

  // linear velocity
  double lin_error = sqrt(dx*dx + dy*dy);
  double v = lin_error;

  // Velocity
  return types::geometry::UnicycleVelocity { v, w };
}

  }
}
