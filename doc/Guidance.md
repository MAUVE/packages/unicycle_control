# Guidance

Compute velocity commands to reach a pose target, while avoiding local obstacles.
Two implementations:
* PotentialField (algorithm from Guerra el al., "Avoiding Local Minima in the Potential Field Method using Input-to-State Stability", Control Engineering Practice, 55, 2016, https://doi.org/10.1016/j.conengprac.2016.07.008)
* SupervisoryControl (algorithm from Guerra el al., "Finite-time obstacle avoidance for unicycle-like robot subject to additive input disturbances", Autonomous Robots, 41(1), 2017, https://doi.org/10.1007/s10514-015-9526-0)

## Input Ports

* *goal* (StatusValue<Point2D>): goal to reach
* *scan* (LaserScan): input laser scan to detect obstacles

## Output Ports

* *command* (UnicycleVelocity): computed command to reach the goal
* *distance_to_goal* (double): remaining distance to the goal (in meters)
* *obstacle_distance* (double): distance to obstacle (in meters)
* *obstacle* (Pose2D): reconstructed obstacle structure

## Properties

### Shell Properties

* *goal_tolerance* (double): distance at which the goal is considered reached (in meters, default 0.2)
* *obstacle_distance* (double): distance at which obstacles are considered (in meters, default 0.8)
* *safety_distance* (double): safety distance to respect (in meters, default 0.8)

### PotentialField Core Properties

* *nu* (double): $\nu$ parameter (default 0.2)
* *psi* (double): $\psi$ parameter (default 0.2)
* *epsilon* (double): $\epsilon$ parameter (default 0.11)
* *alpha* (double): $\alpha$ parameter (default 4.0)

### PotentialField FSM Properties

* *idle_period* (double): period of the state machine when waiting for a new goal (in ns, default 1s)
* *reach_period* (double): period of the state machine when reaching a goal (in ns, default 10ms)

## State Machine

### PotentialField State Machine
<p>
  <img align="center" src="PotentialFieldFSM.png"/>
</p>

* *read_input*: read input ports
* *reach_goal*: compute velocity to reach goal while avoiding obstacles
* *arrived_goal*: stop the robot and write arrived to goal
