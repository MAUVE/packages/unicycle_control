# SafetyPilot

Implements an Emergency Stop if the robot is too close to obstacles.
Otherwise, it saturates the input command according to maximum velocities and forwards to the output.

## Properties

### Shell Properties

* *stop_distance* (double): distance to obstacle at which with stop (in meters, default 0.8)
* *stop_lateral_distance* (double): lateral distance within which obstacles are considered (in meters, default 0.3)
* *max_linear* (double): maximum linear velocity (in m/s, default 1.0)
* *max_angular* (double): maximum angular velocity (in rad/s, default 0.4)
* *allow_emergency_backward* (boolean): indicates if moving backward is allowed in emergency state (default false)

### FSM Properties

* *period* (double): period of the state machine (in ns, default 10ms)

## Input Ports

* *scan* (LaserScan): input laser scan to detect obstacles
* *input* (UnicycleVelocity): input velocity command

## Output Ports

* *output* (UnicycleVelocity): output velocity command
* *obstacle_distance* (double): distance to obstacles

## State Machine

<p>
  <img align="center" src="SafetyPilotFSM.png"/>
</p>

* *read*: read input scan and compute obstacle distance
* *saturate*: read input velocity and saturate it
* *emergency*: set linear velocity to 0 and write
* *safe*: just write saturated input
