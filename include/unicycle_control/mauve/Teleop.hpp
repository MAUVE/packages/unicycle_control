/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Unicycle Control project.
 *
 * MAUVE Unicycle Control is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Unicycle Control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#ifndef MAUVE_UNICYCLE_CONTROL_TELEOP_HPP
#define MAUVE_UNICYCLE_CONTROL_TELEOP_HPP

#include <mauve/runtime.hpp>
#include <mauve/types.hpp>
#include <mauve/base/PeriodicStateMachine.hpp>

namespace unicycle_control {
namespace mauve {

    namespace geo = ::mauve::types::geometry;
    namespace sen = ::mauve::types::sensor;

    struct TeleopShell : public ::mauve::runtime::Shell {
      // Properties
      ::mauve::runtime::Property<int> & linear_axis =
        mk_property("linear_axis", 1);
      ::mauve::runtime::Property<int> & angular_axis =
        mk_property("angular_axis", 2);
      ::mauve::runtime::Property<int> & stop_button =
        mk_property("stop_button", 2);
      ::mauve::runtime::Property<int> & fast_button =
        mk_property("fast_button", 1);
      ::mauve::runtime::Property<bool> & inverse_stop =
        mk_property("inverse_stop", false);
      // Input port
      ::mauve::runtime::ReadPort< ::mauve::runtime::StatusValue<sen::Joy> > & joystick =
        mk_read_port<::mauve::runtime::StatusValue<sen::Joy>>("joystick",
          ::mauve::runtime::StatusValue<sen::Joy> { ::mauve::runtime::DataStatus::NO_DATA,
            sen::Joy() } );
      // Output stop event
      ::mauve::runtime::EventPort & stop = mk_event_port("stop");
      // Output velocity command
      ::mauve::runtime::WritePort< ::mauve::types::geometry::UnicycleVelocity > & command =
        mk_write_port<::mauve::types::geometry::UnicycleVelocity>("command");
    };

    struct TeleopCore : public ::mauve::runtime::Core<TeleopShell> {
      void update();
    };

    using Teleop = ::mauve::runtime::Component<TeleopShell, TeleopCore,
      ::mauve::base::PeriodicStateMachine<TeleopShell, TeleopCore> >;

  }
}

#endif // MAUVE_UNICYCLE_CONTROL_TELEOP_HPP
