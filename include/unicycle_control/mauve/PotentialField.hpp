/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Unicycle Control project.
 *
 * MAUVE Unicycle Control is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Unicycle Control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#ifndef MAUVE_UNICYCLE_CONTROL_PF_HPP
#define MAUVE_UNICYCLE_CONTROL_PF_HPP

#include <mauve/base/PeriodicStateMachine.hpp>
#include "UnicycleShell.hpp"
#include "../PotentialField.hpp"

namespace unicycle_control {
  namespace mauve {

    /** Core of the Potential Field algorithm */
    struct PotentialFieldCore : public ::mauve::runtime::Core<ObstacleAvoidanceShell>,
      public PotentialField {
    public:
      /** Algorithm parameter \mu */
      ::mauve::runtime::Property<double> & nu = mk_property("nu", 0.2);
      /** Algorithm parameter \psi */
      ::mauve::runtime::Property<double> & psi = mk_property("psi", 0.2);
      /** Algorithm parameter \epsilon */
      ::mauve::runtime::Property<double> & epsilon = mk_property("epsilon", 0.11);
      /** Algorithm parameter \alpha */
      ::mauve::runtime::Property<double> & alpha = mk_property("alpha", 4.0);

      // Read inputs
      void read_input_goal();
      // Indicate if a new goal has been received
      inline bool has_goal() { return _has_goal; };
      // Indicate if the robot is arrived at the target
      inline bool is_arrived() { return _arrived; };
      // Update function: computes the next command according to input pose and current target
      void update();
      // stop the robot by sending a null command
      void stop();

      virtual void cleanup_hook() override;

    private:
      ::mauve::types::geometry::Pose2D robot_pose;
      ::mauve::types::geometry::Point2D goal, obstacle;
      bool _has_goal, _arrived;
    };

    struct PotentialFieldFSM : public ::mauve::runtime::FiniteStateMachine< ObstacleAvoidanceShell, PotentialFieldCore > {
      ::mauve::runtime::Property<::mauve::runtime::time_ns_t> & idle_period =
        mk_property("idle_period", ::mauve::runtime::sec_to_ns(1));
      ::mauve::runtime::Property<::mauve::runtime::time_ns_t> & reach_period =
        mk_property("reach_period", ::mauve::runtime::ms_to_ns(10));

      ::mauve::runtime::ExecState<PotentialFieldCore> & read_input
        = mk_execution("read_input", &PotentialFieldCore::read_input_goal);

      ::mauve::runtime::SynchroState<PotentialFieldCore> & sync_wait
        = mk_synchronization("sync_wait", ::mauve::runtime::sec_to_ns(1));

      ::mauve::runtime::ExecState<PotentialFieldCore> & reach_goal
        = mk_execution("reach_goal", &PotentialFieldCore::update);

      ::mauve::runtime::ExecState<PotentialFieldCore> & arrived_goal
        = mk_execution("arrived_goal", &PotentialFieldCore::stop);

      ::mauve::runtime::SynchroState<PotentialFieldCore> & sync_reach
        = mk_synchronization("sync_reach", ::mauve::runtime::ms_to_ns(10));

      bool configure_hook() override {
        set_initial(read_input);

        mk_transition(read_input, &PotentialFieldCore::has_goal, reach_goal);
        set_next(read_input, sync_wait);
        set_next(sync_wait, read_input);

        mk_transition(reach_goal, &PotentialFieldCore::is_arrived, arrived_goal);
        set_next(reach_goal, sync_reach);
        set_next(sync_reach, read_input);

        set_next(arrived_goal, sync_wait);

        sync_wait.set_clock(idle_period.get());
        sync_reach.set_clock(reach_period.get());

        return true;
      };
    };

    using PotentialField = ::mauve::runtime::Component< ObstacleAvoidanceShell,
      PotentialFieldCore, PotentialFieldFSM >;

  }
}


#endif // MAUVE_UNICYCLE_CONTROL_PF_HPP
