/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Unicycle Control project.
 *
 * MAUVE Unicycle Control is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Unicycle Control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#ifndef MAUVE_UNICYCLE_CONTROL_SAFETYPILOT_HPP
#define MAUVE_UNICYCLE_CONTROL_SAFETYPILOT_HPP

#include <mauve/runtime.hpp>
#include <mauve/types.hpp>
#include <mauve/base/PeriodicStateMachine.hpp>

namespace unicycle_control {
  namespace mauve {

    /**
     * Safety Pilot: stops the robot if too close to an obstacle
     */
    struct SafetyPilotShell : public ::mauve::runtime::Shell {
      /** distance at which the robot will be stopped */
      ::mauve::runtime::Property<double> & stop_distance =
        mk_property("stop_distance", 0.8);
      /** ignore obstacle at a lateral distance greater than this property */
      ::mauve::runtime::Property<double> & stop_lateral_distance =
        mk_property("stop_lateral_distance", 0.3);
      /** max linear velocity */
      ::mauve::runtime::Property<double> & max_linear =
        mk_property("max_linear", 1.0);
      /** max angular velocity */
      ::mauve::runtime::Property<double> & max_angular =
        mk_property("max_angular", 0.4);
      /** still allow backward in emergency */
      ::mauve::runtime::Property<bool> & emergency_backward = mk_property("allow_emergency_backward", false);

      /** Input laser scan to detect obstacles */
      ::mauve::runtime::ReadPort< ::mauve::types::sensor::LaserScan > & scan =
        mk_read_port<::mauve::types::sensor::LaserScan>("scan", ::mauve::types::sensor::LaserScan());
      /** Input velocity command */
      ::mauve::runtime::ReadPort< ::mauve::types::geometry::UnicycleVelocity > & input =
        mk_read_port<::mauve::types::geometry::UnicycleVelocity>("input", {0, 0});
      /** Output velocity command */
      ::mauve::runtime::WritePort< ::mauve::types::geometry::UnicycleVelocity > & output =
        mk_write_port<::mauve::types::geometry::UnicycleVelocity>("output");
      /** Output obstacle distance */
      ::mauve::runtime::WritePort< double > & obstacle_distance =
        mk_write_port<double>("obstacle_distance");
    };

    struct SafetyPilotCore : public ::mauve::runtime::Core<SafetyPilotShell> {
      void read_scan();
      void saturate_input();
      bool obs_too_close();
      void stop();
      void forward();
    private:
      double obstacle_distance, obstacle_lateral_distance;
      ::mauve::types::geometry::UnicycleVelocity input;
    };

    struct SafetyPilotFSM : public ::mauve::runtime::FiniteStateMachine<SafetyPilotShell, SafetyPilotCore> {
      ::mauve::runtime::Property<::mauve::runtime::time_ns_t> & period = mk_property("period", ::mauve::runtime::ms_to_ns(10));

      ::mauve::runtime::ExecState<SafetyPilotCore> & read = mk_execution("Read", &SafetyPilotCore::read_scan);
      ::mauve::runtime::ExecState<SafetyPilotCore> & saturate = mk_execution("Saturate", &SafetyPilotCore::saturate_input);
      ::mauve::runtime::ExecState<SafetyPilotCore> & emergency = mk_execution("Emergency", &SafetyPilotCore::stop);
      ::mauve::runtime::ExecState<SafetyPilotCore> & safe = mk_execution("Safe", &SafetyPilotCore::forward);

      ::mauve::runtime::SynchroState<SafetyPilotCore> & sync = mk_synchronization("Sync", ::mauve::runtime::ms_to_ns(10));

      bool configure_hook() override;
    };

    using SafetyPilotComponent = ::mauve::runtime::Component<SafetyPilotShell, SafetyPilotCore, SafetyPilotFSM >;

  }
}

#endif
