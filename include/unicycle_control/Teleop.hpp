/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Unicycle Control project.
 *
 * MAUVE Unicycle Control is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Unicycle Control is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#ifndef UNICYCLE_CONTROL_TELEOP_HPP
#define UNICYCLE_CONTROL_TELEOP_HPP

#include <mauve/types/sensor_types.hpp>

namespace unicycle_control {

  bool buttonPressed(const ::mauve::types::sensor::Joy& j, int b);

  double joystickAxis(const ::mauve::types::sensor::Joy& j, int a);
}

#endif // MAUVE_UNICYCLE_CONTROL_TELEOP_HPP
